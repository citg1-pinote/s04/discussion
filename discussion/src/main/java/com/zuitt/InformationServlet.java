package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class InformationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8880012107939109054L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" InformationServlet has been initialized. ");
		System.out.println("******************************************");
		}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
	PrintWriter out = res.getWriter();
	out.println("<h1>Hotel Information</h1>");
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" InformationServlet has been destroyed. ");
		System.out.println("******************************************");
		}
}
